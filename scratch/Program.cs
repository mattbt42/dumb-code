﻿using System;
using System.ServiceProcess;

namespace scratch
{
    class Program
    {
        static void Main(string[] args)
        {
            var test = ServiceController.GetServices();
            foreach (var service in test)
            {
                Console.WriteLine(service.ServiceName);
            }
        }
    }
}
