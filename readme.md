# Matt's attempt at making a docker container!


## Building
To build this image from this directory, run:

```batch
docker build -t matt-build:latest -m 4GB .
```

## Running
To map and build native sources from a clean source repository, run:

```batch
docker run --name Solution -m 4G -v ${pwd}:C:\src matt-build:latest "msbuild /m c:\src\scratch.sln; vstest.console --settings:settings.runsettings C:\src\**\bin\**\UnitTestProject*.dll"
```

You can optionally pass specific configurations to build as well.

```batch
docker run --name Solution -m 4G -v ${pwd}:C:\src matt-build:latest --name Solution msbuild /m c:\src\scratch.sln /p:Configuration=Debug /p:Platform=x64
```

To build again run the container created in the previous step, e.g.
```batch
docker start -a Solution
```

You can omit the -a that attaches the container to view the output if desired.

### Command line container
``` powershell
docker run -i --name commandline -m 2G -v ${pwd}:C:\src matt-build:latest powershell
```

## Issues

* If the repository is not clean and the mapped directory is not on the same drive or the same path as the host directory, native project builds will fail with a front-end compiler error.
* The compile flag /CI causes a compiler error when used in a container. In your project properties under C/C++ change Debug Information Format to C7 compatible when building in a container.
